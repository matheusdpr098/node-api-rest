const express = require('express');
const app = express();

const routerProduto = require('./routes/produto')

app.use('/produto', routerProduto);
app.use( (req, res,  next) => {
    res.status(200).send({
        message : 'OK chegou no serve'
    });
});

module.exports = app;